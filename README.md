# big_data_scattering

Big data handling for quantum scattering using molscat

## Idea

This project will use molscat with PBS system to produce data for ML models of quantum scattering. We will try to use it for approximate calculations of cross sections. To this end we need to establish the protocol of systematic data mining: systematic calculations for varied potential, snippets for data processing, CSV files.

## What is varied?

Following parameters should be varied:
 - reduced mass (in case of astrochemistry the span of reduced masses is small, from 2 to 4)
 - rotational constant Be
 - energy
 - potential combined from either A exp(-b x )  type exponential components, or x^{-n} representation 
    - C12 , C6 for V0
    - C7 for V1
    - for the moment it is enough .... 

## Molscat entry

We will use simplest possible input for molscat - it is possible to write it down as 
```
&INPUT
    LABEL  = 'comment_xxx ',
    URED   =  muxxx ,
    IPRINT =  12,
    ISIGPR =    1,  IRMSET = 0, 
    RMIN   =   2.0, RMAX   =   300.0,  RMID = 15.0, 
    IPROPS =   7,   IPROPL =       9,    DRS=0.005,  
    NNRG   =  500,   ENERGY =1e-5,5  ,  LOGNRG=TRUE  
    JTOTL  = jxxx,   JTOTU  =  jxxx,     JSTEP = 1,
    LASTIN = 0
    SCALAM = 1
 /

 &BASIS
    ITYPE  = 1,  BE     = bexxx ,  JMAX=15,
 /

 &POTL
    MXLAM = 2,   LAMBDA =   0,   1, 
                 NTERM  =   2,   1, 
                 NPOWER = -12,  -6,   -7,  
                 A      =   C12xxx, -C6xxx,
                           -C7xxx  
                 RM    = 1.0, EPSIL  =  1.0,
 /


```
Molscat 2020 is used, which differs significantly  from previous versions

***
