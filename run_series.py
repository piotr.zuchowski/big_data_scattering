import os
import re


ured=4.0
energy=1e-3
brot=0.5

c12=3318059823.5743217
c6 =963875.9
c7 =154220.144

komentarz = ' 1muK - skalowanie potentjalu lekko anizotropowego lekki uklad  '

inputtxt="""
&INPUT
    LABEL  = 'comment_xxx ',
    URED   =  muxxx ,
    IPRINT =  12,
    ISIGPR =    1,  IRMSET = 0, 
    RMIN   =   3.0, RMAX   =   300.0,  RMID = 15.0, 
    IPROPS =   7,   IPROPL =       9,    DRS=0.01,  
    NNRG   =   1,   ENERGY = enerxxx ,    
    JTOTL  =  0,   JTOTU  =  20,     JSTEP = 1,
    LASTIN = 0
    SCALAM = scalxxx
 /

 &BASIS
    ITYPE  = 1,  BE     = bexxx ,  JMAX=15,
 /

 &POTL
    MXLAM = 2,   LAMBDA =   0,   1, 
                 NTERM  =   2,   1, 
                 NPOWER = -12,  -6,   -7,  
                 A      =   C12xxx, -C6xxx,
                           -C7xxx  
                 RM    = 1.0, EPSIL  =  1.0,
 /


"""
inputtxt=inputtxt.replace('muxxx',str(ured))
inputtxt=inputtxt.replace('enerxxx',str(energy))
inputtxt=inputtxt.replace('bexxx',str(brot))

inputtxt=inputtxt.replace('C12xxx',str(c12))
inputtxt=inputtxt.replace('C6xxx',str(c6))
inputtxt=inputtxt.replace('C7xxx',str(c7))

inputtxt=inputtxt.replace('comment_xxx',komentarz)
print(inputtxt)

def frange(start, stop=None, step=None):
    # if set start=0.0 and step = 1.0 if not specified
    start = float(start)
    if stop == None:
        stop = start + 0.0
        start = 0.0
    if step == None:
        step = 1.0

    print("start = ", start, "stop = ", stop, "step = ", step)

    count = 0
    while True:
        temp = float(start + count * step)
        if step > 0 and temp >= stop:
            break
        elif step < 0 and temp <= stop:
            break
        yield temp
        count += 1

for iscal in frange(0.95,1.05,0.001):
	nameoffile="scal_"+str(energy)+'_'+str(iscal)
	input_text = inputtxt
	input_text=input_text.replace('scalxxx',str(iscal))
	file1 = open(nameoffile,"w")
	file1.write(input_text)
	file1.close()
 	os.system('/home/pzuch007/bin/run_molscat.sh  '+nameoffile)
